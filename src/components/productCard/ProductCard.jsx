import './ProductCard.scss';
import React, {Component, useState} from "react";
import Button from "../button/Button";
import { faStar } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from "prop-types";


const ProductCard = (props) => {

    const [isProductLiked, setIsProductLiked] = useState(props.liked || false)

    const { id, name, price, imageUrl, color } = props.product
    const { activateModal, toggleLiked } = props

    return (

        <div className="card">

            <div className="cardImage"
                 style={{
                     backgroundImage: `url(${imageUrl})`
                 }}>
                <FontAwesomeIcon
                    icon={ faStar }
                    onClick={() => {
                        toggleLiked(props.product, isProductLiked);
                        setIsProductLiked(!isProductLiked)
                    }}
                    style={{
                        color: `${isProductLiked ? "#1165B2" : "#fff"}`
                    }}

                />
            </div>

            <h3 key={name}>{name}</h3>

            <div className="cardColor">
                <p>Available color: </p>
                <div style={{ backgroundColor: `${color}` }}/>
            </div>

            <p className="cardPrice" key={id}>$ {price}</p>

            <Button
                text="Add to cart"
                onClick={() => {
                    activateModal(props.product)
                }}
            />

        </div>
    )
}


export default ProductCard;

ProductCard.propTypes = {
    product: PropTypes.shape({
        code: PropTypes.number,
        color: PropTypes.string,
        id: PropTypes.number,
        imageUrl: PropTypes.string,
        price: PropTypes.number
    }).isRequired,
    activateModal: PropTypes.func.isRequired,
    toggleLiked: PropTypes.func.isRequired

}

import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCartShopping, faStar } from "@fortawesome/free-solid-svg-icons"
import { Link } from "react-router-dom";

import './Header.scss';

const Header = (props) => {

        return(
            <>
                <div className="header">
                    <Link to="/">
                        <div className="headerTitle">
                            <h1>Kosher</h1>
                            <h2>motorcycles</h2>
                        </div>
                    </Link>
                    <div className="headerFiller" />
                    <div className="headerCart">
                        <Link to="/cart">
                            <FontAwesomeIcon icon={ faCartShopping } />
                            {props.cart.length > 0 && <div className="headerQuantity">{props.cart.length}</div>}
                        </Link>
                    </div>
                    <div className="headerWishList">
                        <Link to="/favorites">
                            <FontAwesomeIcon icon={ faStar } />
                            {props.liked.length > 0 && <div className="headerQuantity">{props.liked.length}</div>}
                        </Link>
                    </div>
                </div>
                <div className="headerLine"></div>
                <div className="headerNav">
                    <Link to="/">Home</Link>
                    <Link to="/cart">Cart</Link>
                    <Link to="/favorites">Favorites</Link>
                </div>
            </>
        )
}

export default Header;

Header.propTypes = {
    cart: PropTypes.array,
    liked: PropTypes.array,
}

Header.defaultProps = {
    cart: [],
    liked: []
}
